var express = require('express');
var router = express.Router();
var User = require('../models/user');
var Item = require('../models/create_item');
var Comment = require('../models/comment');
var path = require('path')
var app = express();  
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var mongodb = require('mongodb')
var formidable = require('formidable');
var fs = require('fs');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb+srv://304cem:Blacklotus123@cluster0-gfbbm.azure.mongodb.net/test?retryWrites=true&w=majority";
app.set('view engine', 'ejs');  
mongoose.connect('mongodb+srv://304cem:Blacklotus123@cluster0-gfbbm.azure.mongodb.net/304cem');

var username = "";
router.use(bodyParser.urlencoded({extended: false}));
// GET route for homepage
router.get('/', function (req, res, next) {
  return res.sendFile(path.join(__dirname + '/views/index.html'));
  
});

//POST route for updating data
router.post('/', function (req, res, next) {
  
  if (req.body.password !== req.body.passwordConf) {
    var err = new Error('Password doesn\'t match!');
    err.status = 400;
    res.send('Password doesn\'t match!');
    return next(err);
  }

  if (req.body.email &&
    req.body.username &&
    req.body.password &&
    req.body.passwordConf) {

    var userData = {
      email: req.body.email,
      username: req.body.username,
      password: req.body.password,
      passwordConf: req.body.passwordConf,
    }

    User.create(userData, function (error, user) {
      if (error) {
        return next(error);
      } else {
        req.session.userId = user._id;
        return res.redirect('/main');
      }
    });

  } else if (req.body.logemail && req.body.logpassword) {
    User.authenticate(req.body.logemail, req.body.logpassword, function (error, user) {
      if (error || !user) {
        var err = new Error('Wrong email or password!');
        err.status = 401;
        return next(err);
      } else {
        req.session.userId = user._id;
        return res.redirect('/main'); // go to the page
      }
    });
  } else {
    var err = new Error('All fields are required!');
    err.status = 400;
    return next(err);
  }
})

// GET route to redirect to '/profile' page after registering
router.get('/main', function (req, res, next) {
  User.findById(req.session.userId)
    .exec(function (error, user) {
      if (error) {
        return next(error);
      } else {
        if (user === null) {
          var err = new Error('Not authorized! Go back!');
          err.status = 400;
          return next(err);
        } else {
         
          username = user.username;
         // res.sendFile(path.join(__dirname , '../views/main.html'));
         res.render(path.join(__dirname , '../views/main.ejs'), {name:username} );
         // res.render(path.join(__dirname , '../views/main.html'), {name:username})
          

          //return res.send('<h2>Your name: </h2>' + user.username + '<h2>Your email: </h2>' + user.email + '<br><a type="button" href="/logout">Logout</a>')
        }
      }
    });
});

router.get('/create_item', function (req, res, next){
  res.render(path.join(__dirname , '../views/create_item.ejs'), {name:username});
})

var image = ""





router.post("/create", (req, res) => {
  var item_data = {
    username: req.body.username,
    image: req.body.image,
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
    location: req.body.location,
    phone: req.body.phone
  }
Item.create(item_data, function (error, user) {
  if (error) {
    return next(error);
  } else {
    return res.redirect('/main');
  }
});

})


router.post('/upload', (req, res) => {
  var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
      image = files.filetoupload.name;
      var oldpath = files.filetoupload.path;
      var newpath = path.join(__dirname , '../public/image/' + files.filetoupload.name ) ;
      fs.rename(oldpath, newpath, function (err) {
        if (err) throw err;
        res.end;
      });
 });
})

router.get('/all_item', (req, res) => {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("304cem");
    dbo.collection('items').find({}).toArray(function (err, result) {
      if (err) throw err;
      res.json(result);
      db.close();
    });

  });
});

var product_id = "";
var created_by = "";
var product_image = "";
var product_name = "";
var description = "";
var price = "";
var location = "";
var phone = "";

router.post('/one_item', function (req, res) {
  //res.render(path.join(__dirname , '../views/detail.ejs'), {name:username});
  //res.send( req.body.id );
  product_id = req.body.product_id;
  created_by = req.body.created_by;
  product_image = req.body.product_image;
  product_name = req.body.product_name;
  description = req.body.description;
  price = req.body.price;
  location = req.body.location;
  phone = req.body.phone;
  res.render(path.join(__dirname , '../views/detail.ejs') , {name:username, product_id:product_id,created_by:created_by,product_image:product_image,product_name:product_name,description:description,price:price,location:location,phone:phone });
});

router.delete('/delete_item', function (req, res) {
  var item_id = req.body.item_id;

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("304cem");
    var delete_item = { _id: new mongodb.ObjectID(item_id)};
    dbo.collection("items").deleteOne(delete_item, function(err, obj) {
      if (err) throw err;
      
      db.close();
    });
  });
  res.redirect('/main');
});



router.post('/update_item', function (req, res) {
  var item_id = req.body.item_id;
  var description = req.body.description;
  var price = req.body.price;
  var location = req.body.location;
  var phone = req.body.phone;
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("304cem");
    var myquery = { _id: new mongodb.ObjectID(item_id) };
    var newvalues = { $set: {description: description, price: price,location:location,phone:phone } };
    dbo.collection("items").updateOne(myquery, newvalues, function(err, res) {
      if (err) throw err;
      db.close();
    });
    
  });

  res.redirect('/main');

})

router.post("/comment", function(req, res) {
  var data = { 
    comment: req.body.comment,
    product_id: req.body.product_id,
    username:req.body.username 
    };
    Comment.create(data, function (error) {
      if (error) {
        return next(error);
      }else{

      }

    });
})

router.get('/all_comment', (req, res) => {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("304cem");
    dbo.collection('comments').find({}).toArray(function (err, result) {
      if (err) throw err;
      res.json(result);
      db.close();
    });

  });
});

router.post('/delete_comment', function (req, res) {
  var comment_id = req.body.comment_id;

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("304cem");
    var delete_item = { _id: new mongodb.ObjectID(comment_id)};
    dbo.collection("comments").deleteOne(delete_item, function(err, obj) {
      if (err) throw err;
      
      db.close();
    });
  });
  //return res.redirect('/main');
});


router.post('/change_comment', function (req, res) {
  var selected_id = req.body.selected_id;
  var change_comment = req.body.change_comment;
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("304cem");
    var myquery = { _id: new mongodb.ObjectID(selected_id) };
    var newvalues = { $set: {comment: change_comment} };
    dbo.collection("comments").updateOne(myquery, newvalues, function(err, res) {
      if (err) throw err;
      db.close();
    });
    
  });


})

// GET for logout
router.get('/logout', function (req, res, next) {
  if (req.session) {
    // delete session object
    req.session.destroy(function (err) {
      if (err) {
        return next(err);
      } else {
        return res.redirect('/');
      }
    });
  }
});

module.exports = router;